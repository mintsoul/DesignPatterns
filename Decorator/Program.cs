﻿using System;
using Decorator.Decorators;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            var mede_wm = new DryDecorator(new DisinfectDecorator(new MedeWashingMachine()));
            mede_wm.Washing();
            mede_wm.CompletedNotify();

            Console.WriteLine();

            var swan_wm = new DryDecorator(new DisinfectDecorator(new SwanWashingMachine()));
            swan_wm.Washing();
            swan_wm.CompletedNotify();
        }
    }
}
