﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public class SwanWashingMachine : WashingMachine
    {
        public override void Washing()
        {
            Console.WriteLine("天鹅牌洗衣机洗衣服...");
        }

        public override void CompletedNotify()
        {
            Console.WriteLine("天鹅：衣服洗好了！");
        }
    }
}
