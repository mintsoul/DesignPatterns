﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public class MedeWashingMachine : WashingMachine
    {
        public override void Washing()
        {
            Console.WriteLine("美的洗衣机洗衣服...");
        }

        public override void CompletedNotify()
        {
            Console.WriteLine("美的：衣服洗好了！");
        }
    }
}
