﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator.Decorators
{
    public class DisinfectDecorator : Decorator
    {
        private WashingMachine _washingMachine;
        public DisinfectDecorator(WashingMachine washingMachine)
            :base(washingMachine)
        {
            _washingMachine = washingMachine;
        }
        public override void Washing()
        {
            BeforeWashing();

            _washingMachine.Washing();

            Disinfect();
        }

        public override void CompletedNotify()
        {
            Console.WriteLine("消毒完成了！");
        }

        private void Disinfect()
        {
            Console.WriteLine("洗衣过程中对衣服进行消毒...");
        }

        private void BeforeWashing()
        {
            Console.WriteLine("洗衣服前先通电、加水...");
        }
    }
}
