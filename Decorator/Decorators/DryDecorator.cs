﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator.Decorators
{
    public class DryDecorator : Decorator
    {
        private WashingMachine _washingMachine;

        public DryDecorator(WashingMachine washingMachine)
            :base(washingMachine)
        {
            _washingMachine = washingMachine;
        }

        public override void Washing()
        {            
            _washingMachine.Washing();

            DrainAwayWater();
            Drying();
        }

        public override void CompletedNotify()
        {
            Console.WriteLine("烘干完成了！");
        }

        private void Drying()
        {
            Console.WriteLine("洗衣服结束后进行烘干...");
        }

        private void DrainAwayWater()
        {
            Console.WriteLine("烘干前排水");
        }
    }
}
