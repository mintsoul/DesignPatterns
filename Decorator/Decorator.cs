﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public abstract class Decorator : WashingMachine
    {
        private WashingMachine _washingMachine;

        public Decorator(WashingMachine washingMachine) => _washingMachine = washingMachine;

        public abstract override void Washing();
    }
}
