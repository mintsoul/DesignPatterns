﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Decorator
{
    public abstract class WashingMachine
    {
        public abstract void Washing();

        public abstract void CompletedNotify();
    }
}
