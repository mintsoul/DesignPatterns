﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyWarBook
{
    public class King : IPerson
    {
        public void Recieve()
        {
            Console.WriteLine("本王已经收到战书，即日应战。谁怕谁！");
        }
    }
}
