﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyWarBook
{
    public class KingProxy : IPerson
    {
        private King _king;

        public KingProxy(King king) => _king = king;

        public void Recieve()
        {
            if (SafeCheck())
            {
                Console.WriteLine("检查通过！");
                _king.Recieve();
            }
            else
            { Reject(); }

        }

        private bool SafeCheck()
        {
            Console.WriteLine("战书安全检查。。。");
            if (new Random().Next(1,9) > 4)
            {
                return false;
            }

            return true;
        }

        private void Archive()
        {
            Console.WriteLine("已经将战书存档。");
        }

        private void Reject()
        {
            Console.WriteLine("战书造假，焚烧战书。");
        }
    }
}
