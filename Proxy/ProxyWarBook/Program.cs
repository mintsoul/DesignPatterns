﻿using System;

namespace ProxyWarBook
{
    class Program
    {
        static void Main(string[] args)
        {     
            var king = new KingProxy(new King());

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("派大使快马加鞭向东吴传达战书....");

                king.Recieve();

                Console.WriteLine("\n");
            }            
        }
    }
}
