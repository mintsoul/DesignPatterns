﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPoisonTea
{
    interface IDrink
    {
        void Drink();

        void Open();

        string GetShape();

        string GetTaste();
    }
}
