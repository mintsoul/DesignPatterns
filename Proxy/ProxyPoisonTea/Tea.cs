﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPoisonTea
{
    public class Tea : IDrink
    {
        public Tea Drink()
        {
            Console.WriteLine("Drink the tea.");
        }

        public void Open()
        {
            Console.WriteLine("Open the tea cup");
        }

        public string GetShape()
        {
            return "this tea is triangle";
        }

        public string GetTaste()
        {
            return "it is very delicious!";
        }

        public bool IsPoison()
        {
            if (new Random().Next(1, 9) > 4)
                return true;
            else
                return false;
        }

        public string name { get; set; } = "";

        public string color { get; set; } = "green";

        public int weight { get; set; } = 20;        
    }
}
