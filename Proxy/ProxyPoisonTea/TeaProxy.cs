﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPoisonTea
{
    public class TeaProxy : IDrink
    {
        private Tea _tea;
        private bool isOk;

        public TeaProxy(string TeaName)
        {
            _tea.name = TeaName;
            isOk = false;
        }

        public Tea Drink()
        {
            if (!isOk)
            {
                _tea = new Tea()
                {
                    name = _tea.name,
                    color = "black"
                };

                return _tea;
            }

            return 
        }

        public string GetShape()
        {
            Console.WriteLine("给茶叶的形状加点点缀");
            return _tea.GetShape();
        }

        public string GetTaste()
        {
            Console.WriteLine("先加入一点不是很毒的毒药");
            return _tea.GetTaste();
        }

        public void Open()
        {
            throw new NotImplementedException();
        }
    }
}
