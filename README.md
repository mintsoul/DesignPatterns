# DesignPatterns

#### 介绍
记录设计模式的学习code，便于复习。先学习一轮code一轮再回过头来把代码注释写上。

#### 设计模式
1. 装饰者模式：Decorator
2. 代理模式：Proxy
